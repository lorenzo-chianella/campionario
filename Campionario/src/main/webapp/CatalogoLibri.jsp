<%@page import="it.pegaso.campionario.model.Articolo"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Ecco i nostri Libri</title>
</head>
<body>
	<h1 style="text-align: center; color: red;">Lista dei nostri libri</h1>
	<table align="center">
	<thead>
		<tr>
			<th style="width:20em; border: 1px solid black">Titolo</th>
			<th style="width:20em; border: 1px solid black">Autore</th>
			<th style="width:20em; border: 1px solid black">Genere</th>
		</tr>
	</thead>
	<tbody>
		<% List<Articolo> list = (List<Articolo>)request.getAttribute("list"); %>	
		<% for (Articolo a : list) {
			if (a.isLibroOrCd()){
		%>	
		<tr>
			<td style="width:20em; border: 1px solid black"><%= a.getTitolo()%></td>
			<td style="width:20em; border: 1px solid black"><%= a.getAutore()%></td>
			<td style="width:20em; border: 1px solid black"><%= a.getGenere()%></td>
		</tr> 
		
		<%
			}
		}
		%>	
	</tbody>
	</table>
	<form action="LibroById" >
		<p>Inserire l'ID del libro da cercare:</p>
		<input type="search" name="id">
		<input type="submit" >
	</form>
</body>
</html>