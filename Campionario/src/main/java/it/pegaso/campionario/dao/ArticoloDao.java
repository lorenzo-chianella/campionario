package it.pegaso.campionario.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.pegaso.campionario.model.Articolo;

public class ArticoloDao {

	private final String CONNECTION_STRING = "jdbc:mysql://localhost:3306/libroocd?user=root&password=root&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	private Connection connection;
	private PreparedStatement getLibroById;
	private PreparedStatement getCdById;
	
	public List<Articolo> getAll(){
		List<Articolo> result = new ArrayList<Articolo>();
		ResultSet rs;
		try {
			rs = getConnection().createStatement().executeQuery("select * from campionario");
			while(rs.next()) {
				Articolo a = new Articolo();
				a.setId(rs.getLong("id"));
				a.setTitolo(rs.getString("titolo"));
				a.setAutore(rs.getString("autore"));
				a.setGenere(rs.getString("genere"));
				a.setAnno(rs.getString("anno"));
				a.setLibroOrCd(rs.getBoolean("isLibroOrCd"));
				a.setQuantita(rs.getInt("quantita"));
				result.add(a);
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}
	
	public Articolo getLibro(int id) {
		Articolo libro = null;
		try {
			getGetLibroById().clearParameters();
			getGetLibroById().setInt(1, id);
			ResultSet rs = getGetLibroById().executeQuery();
			
			if (rs.next()){
				libro = new Articolo();
				libro.setId(rs.getInt("id"));
				libro.setTitolo(rs.getString("titolo"));
				libro.setAutore(rs.getString("autore"));
				libro.setGenere(rs.getString("genere"));
				libro.setAnno(rs.getString("anno"));
				libro.setLibroOrCd(rs.getBoolean("isLibroOrCd")); //1 è libro
				libro.setQuantita(rs.getInt("quantita"));
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return libro;
	}
	
	public Articolo getCD(int id) {
		Articolo cd = null;
		try {
			getGetCdById().clearParameters();
			getGetCdById().setInt(1, id);
			ResultSet rs = getGetCdById().executeQuery();
			
			if (rs.next()){
				cd = new Articolo();
				cd.setId(rs.getInt("id"));
				cd.setTitolo(rs.getString("titolo"));
				cd.setAutore(rs.getString("autore"));
				cd.setGenere(rs.getString("genere"));
				cd.setAnno(rs.getString("anno"));
				cd.setLibroOrCd(rs.getBoolean("isLibroOrCd")); //1 è libro 0 è cd
				cd.setQuantita(rs.getInt("quantita"));
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return cd;
	}
	
	
	public Connection getConnection() throws SQLException, ClassNotFoundException {
		if(connection == null) {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection(CONNECTION_STRING);
		}
		return connection;
	}
	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	public PreparedStatement getGetLibroById() throws SQLException, ClassNotFoundException {
		if(getLibroById == null) {
			getLibroById = getConnection().prepareStatement("SELECT * FROM campionario WHERE isLibroOrCd = 1 AND id = ?");
		}
		return getLibroById;
	}
	public void setGetLibroById(PreparedStatement getLibroById) {
		this.getLibroById = getLibroById;
	}
	public PreparedStatement getGetCdById() throws ClassNotFoundException, SQLException {
		if (getCdById == null) {
			getCdById = getConnection().prepareStatement("SELECT * FROM campionario WHERE isLibroOrCd = 0 AND id = ?");
		}
		return getCdById;
	}
	public void setGetCdById(PreparedStatement getCdById) {
		this.getCdById = getCdById;
	}


}
