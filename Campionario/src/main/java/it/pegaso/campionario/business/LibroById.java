package it.pegaso.campionario.business;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.pegaso.campionario.dao.ArticoloDao;
import it.pegaso.campionario.model.Articolo;

/**
 * Servlet implementation class LibroById
 */
public class LibroById extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LibroById() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ArticoloDao dao = new ArticoloDao();
		Articolo result = new Articolo();
		String index = request.getParameter("id");
		int id = Integer.parseInt(index);
		result = dao.getLibro(id);
		request.setAttribute("articolo", result);
		RequestDispatcher rd = request.getRequestDispatcher("LibroCercato.jsp");
		rd.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
