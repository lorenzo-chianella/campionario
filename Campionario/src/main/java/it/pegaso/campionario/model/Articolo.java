package it.pegaso.campionario.model;

public class Articolo {

	private long id;
	private String titolo;
	private String autore;
	private String genere;
	private String anno;
	private boolean isLibroOrCd; //0 è cd, 1 è libro
	private int quantita;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitolo() {
		return titolo;
	}
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	public String getAutore() {
		return autore;
	}
	public void setAutore(String autore) {
		this.autore = autore;
	}
	public String getGenere() {
		return genere;
	}
	public void setGenere(String genere) {
		this.genere = genere;
	}
	public String getAnno() {
		return anno;
	}
	public void setAnno(String anno) {
		this.anno = anno;
	}
	public boolean isLibroOrCd() {
		return isLibroOrCd;
	}
	public void setLibroOrCd(boolean isLibroOrCd) {
		this.isLibroOrCd = isLibroOrCd;
	}
	public int getQuantita() {
		return quantita;
	}
	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}
	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		
		String tipoArticolo = "";
		
		if(isLibroOrCd()) {
			//1 = true = libro
			tipoArticolo = "|Libro|";
		} else {
			tipoArticolo = "|CD|";
		}
		sb.append("Articolo: ");
		sb.append(getTitolo() + " ");
		sb.append("by: ");
		sb.append(getAutore() + " ");
		sb.append(tipoArticolo);
		
		return sb.toString();
	}

	
}

